package entidades.personaje

import entidades.item.InventarioItem
import entidades.lugares.Lugar
import entidades.stats.Stats
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import org.litote.kmongo.Id
import org.litote.kmongo.newId

@Serializable
data class Personaje( val nombre :String, val cuenta: String,val data:PersonajeData, var lugar : Lugar,val inventario :MutableList<InventarioItem>, @Contextual val _id: Id<Personaje> = newId()){

}