package entidades.personaje

import entidades.item.InventarioItem
import entidades.item.Item
import entidades.item.ItemIdCantidad
import entidades.lugares.Lugar
import entidades.lugares.LugarController
import entidades.personaje.db.PersonajeDAOMongo
import entidades.personaje.db.PersonajeTOMongo
import entidades.stats.Stats
import org.litote.kmongo.Id
import util.randomNormal25Porciento
import util.randomNormalInt
import kotlin.math.ceil
import kotlin.math.floor

class PersonajeController {
    val personajeDb = PersonajeDAOMongo()
    val lugarController = LugarController()
    fun buscarPersonajeFromCuenta(cuenta: String):Personaje?{
        return personajeDb.getPersonajeFromCuenta(cuenta)
    }

    fun crearPersonaje(nombre :String, cuenta:String):Personaje?{
        val lugar = lugarController.getLugarInicial()
        val p = Personaje(nombre,cuenta, PersonajeData(1,0,500,Stats(25,10,10)),lugar!!, mutableListOf(InventarioItem(Item(1,"Tarjeta de identificacion","1"),1)))
        if(personajeDb.addPersonaje(p)){

            return personajeDb.getPersonajeByNombre(nombre)
        }
        return null

       // listaPersonaje.addPersonaje(Personaje(listaPersonaje.getNextId(),nombre,cuentaLista.buscarUser(cuenta)?.id!!,500,1))
    }
    fun trabajar(personaje: Personaje):Int{
        val num = randomNormalInt(350,100)
        personaje.data.oro += num
        personajeDb.updateOro(personaje._id,personaje.data.oro)
        return num
    }


    fun viajar(personaje: Personaje,lugar: Int):Boolean{
            return personajeDb.updateLugar(personaje._id, lugar)
    }
    fun reestablecer(origen: Personaje,vida:Boolean = false){
        if(vida) origen.data.statsActuales.vida = origen.data.stats.vida
        origen.data.statsActuales.ataque = origen.data.stats.ataque
        origen.data.statsActuales.defensa = origen.data.stats.defensa
    }

    /**
     * @return Pair(experiencia, oro)
     */

    fun getRecompensaExp(personaje: Personaje,recompensa:Int):Pair<Boolean,Int>{
        val exp = randomNormal25Porciento (ceil(recompensa*0.75).toInt())
        val t =levelUp(personaje,exp)
        return Pair(t,exp)
    }
    fun levelUp(personaje: Personaje,exp:Int):Boolean{
        personaje.data.experiencia += exp
        val nuevoNivel = personaje.data.experiencia /500 + 1
        if(personaje.data.nivel <nuevoNivel){
            personaje.data.nivel = nuevoNivel
            return true
        }
        return false
    }
    fun getRecompensaOro(personaje: Personaje,recompensa:Int):Int{
        val oro = randomNormal25Porciento(recompensa)
        personaje.data.oro += oro
        return oro
    }
    fun updateData(personaje: Personaje):Boolean{
        return personajeDb.updateData(personaje)
    }

    fun borrar(personaje: Personaje):Boolean{
        return personajeDb.delete(personaje._id)
    }

}