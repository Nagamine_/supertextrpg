package entidades.personaje

import entidades.lugares.Lugar
import entidades.stats.Stats
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable

@Serializable
data class PersonajeData (var nivel:Int, var experiencia:Int, var oro:Int, var stats: Stats){
    val statsActuales:Stats = stats.copy()
}