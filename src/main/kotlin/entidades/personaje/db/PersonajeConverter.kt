package entidades.personaje.db

import entidades.item.InventarioItem
import entidades.item.ItemIdCantidad
import entidades.personaje.Personaje

object PersonajeConverter {
    fun entidad2Modelo(personaje: Personaje):PersonajeTOMongo{
        val inventario = mutableListOf<ItemIdCantidad>()
        personaje.inventario.forEach{
            inventario.add(ItemIdCantidad(it.item._id,it.cantidad))
        }
        return PersonajeTOMongo(personaje.nombre,personaje.cuenta,personaje.data,personaje.lugar._id,inventario, personaje._id)
    }
}