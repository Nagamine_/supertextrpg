package entidades.personaje.db

import entidades.item.ItemIdCantidad
import entidades.lugares.Lugar
import entidades.personaje.Personaje
import entidades.personaje.PersonajeData
import entidades.stats.Stats
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import org.litote.kmongo.Id
import org.litote.kmongo.newId

@Serializable
data class PersonajeTOMongo (val nombre :String, val cuenta: String, var data: PersonajeData,var lugarId: Int, val inventario:List<ItemIdCantidad>, @Contextual val _id: Id<Personaje> = newId())