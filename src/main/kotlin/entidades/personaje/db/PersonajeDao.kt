package entidades.personaje.db

import entidades.personaje.Personaje
import org.litote.kmongo.Id

interface PersonajeDao {
    fun getTodos():Collection<Personaje>
    fun getPersonajeById(id: Id<Personaje>): Personaje?
    fun getPersonajeByNombre(nombre:String): Personaje?
    fun getPersonajeFromCuenta(cuenta:String): Personaje?
    fun addPersonaje(personaje: Personaje):Boolean
    fun updateLugar(personaje: Id<Personaje>,lugar:Int):Boolean
    fun updateOro(personaje: Id<Personaje>, oro :Int):Boolean
    fun updateExp(personaje: Personaje):Boolean
    fun updateData(personaje: Personaje):Boolean
    fun delete(personaje: Id<Personaje>):Boolean
}