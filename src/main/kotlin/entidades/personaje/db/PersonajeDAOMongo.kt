package entidades.personaje.db

import aTestDB.Prub
import aTestDB.PrubInPrub
import database.Mongo
import entidades.personaje.Personaje
import entidades.personaje.PersonajeData
import org.bson.BsonDocument
import org.bson.Document
import org.litote.kmongo.*
import org.litote.kmongo.MongoOperator.*

class PersonajeDAOMongo : PersonajeDao {
    private val personajeDB = Mongo.getDatabase().getCollection<Personaje>()
    private val personajeDBTO = Mongo.getDatabase().getCollection<PersonajeTOMongo>("personaje")
    private val queryInventario = lookup("item","inventario.itemId","_id","inventario")
    private val queryLugar = lookup("lugar","lugarId","_id","lugar" )


    override fun getTodos(): Collection<Personaje> {
        return personajeDB.aggregate<Personaje>(select()).toList()

    }

    override fun getPersonajeById(id: Id<Personaje>): Personaje? {
        return personajeDB.aggregate<Personaje>(select("_id",id.toString())).first()

    }

    override fun getPersonajeByNombre(nombre: String): Personaje? {
        return personajeDB.aggregate<Personaje>(select("nombre",nombre)).first()

    }

    override fun getPersonajeFromCuenta(cuenta: String): Personaje? {
        return personajeDB.aggregate<Personaje>(select("cuenta",cuenta)).first()

    }

    override fun addPersonaje(personaje: Personaje) :Boolean {
      return personajeDBTO.insertOne(PersonajeConverter.entidad2Modelo(personaje)).wasAcknowledged()
    }

    override fun updateLugar(personaje: Id<Personaje>, lugar: Int): Boolean {
        return personajeDBTO.updateOne(Personaje::_id eq personaje , setValue(PersonajeTOMongo::lugarId,lugar)).wasAcknowledged()
    }
    override fun updateOro(personaje: Id<Personaje>, oro :Int):Boolean{
        return personajeDBTO.updateOne(Personaje::_id eq personaje , setValue(PersonajeTOMongo::data / PersonajeData::oro,oro)).wasAcknowledged()
    }

    override fun updateExp(personaje: Personaje): Boolean {
        return personajeDBTO.updateOne(Personaje::_id eq personaje._id , listOf(setValue(PersonajeTOMongo::data / PersonajeData::nivel,personaje.data.nivel), setValue(PersonajeTOMongo::data / PersonajeData::experiencia,personaje.data.experiencia))).wasAcknowledged()
    }
    override fun updateData(personaje: Personaje): Boolean {
        return personajeDBTO.updateOne(Personaje::_id eq personaje._id , setValue(PersonajeTOMongo::data ,personaje.data)).wasAcknowledged()
    }

    override fun delete(personaje: Id<Personaje>): Boolean {
        return personajeDBTO.deleteOne(Personaje::_id eq personaje).wasAcknowledged()
    }
    private fun select(campo:String,valor:String):String{
        val select = "$campo : \"$valor\""
        return generatequery(select)
    }
    private fun select():String{
        return generatequery()
    }
    fun generatequery(q :String=""):String{

        return """
    [
     {$match: {
         $q
         }
    },
     {$lookup: {
        from: 'lugar',
        localField: 'lugarId',
        foreignField: '_id',
        as: 'lugar'
    }}, 
    {$unwind: {path:"$ lugar"}}, 
    {$unwind: { path: "$ inventario", preserveNullAndEmptyArrays: true }}, 
    {$ addFields: {"inventario":  { "$ifNull" : [ "$ inventario", [ ] ] }}}, 
    {$lookup: {
        from: 'item',
        localField: 'inventario.itemId',
        foreignField: '_id',
        as: "inventario.item"
    }}, 
    {$unwind: { path: "$ inventario.item"}},
    {$group: {
        "_id":"$ _id",
        "inventario":{"$push":"$ inventario"}, 
        "doc":{"$first":"$ $ ROOT"}}  },
    {$replaceRoot: {
        newRoot:{  $ mergeObjects: [ "$ doc",{ inventario: '$ inventario' },]}}}, 
    {$project: {
        "inventario.itemId":0
    }}
    ]""".formatJson()
    }
}