package entidades.stats

import kotlinx.serialization.Serializable

@Serializable
data class Stats(var vida:Int,var ataque:Int,var defensa:Int) {
    constructor(stats:Stats):this(stats.vida,stats.ataque,stats.defensa)
}