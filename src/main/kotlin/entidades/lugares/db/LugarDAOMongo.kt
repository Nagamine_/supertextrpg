package entidades.lugares.db

import database.Mongo
import entidades.lugares.Lugar
import org.bson.Document
import org.bson.conversions.Bson
import org.litote.kmongo.`in`
import org.litote.kmongo.eq
import org.litote.kmongo.findOne
import org.litote.kmongo.getCollection

class LugarDAOMongo : LugarDAO{
    private val lugarDb = Mongo.getDatabase().getCollection<Lugar>()
    override fun getTodos(): Collection<Lugar> {
        return lugarDb.find().toList()
    }

    override fun getById(int: Int): Lugar? {
        return lugarDb.findOne(Lugar::_id eq int)
    }

    override fun getByNombre(name: String): Lugar? {

        return lugarDb.findOne ("{nombre:/^$name\$/i}") //case-insensitive

    }
    override fun getLimites(lugar:Lugar):List<Lugar>{
        return lugarDb.find(Lugar::_id `in` lugar.limites).toList()
    }
}