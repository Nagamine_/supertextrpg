package entidades.lugares.db

import entidades.lugares.Lugar

interface LugarDAO {
    fun getTodos():Collection<Lugar>
    fun getById(int:Int): Lugar?
    fun getByNombre(name: String): Lugar?
    fun getLimites(lugar: Lugar): List<Lugar>
}