package entidades.lugares

import entidades.enemigo.Enemigo
import entidades.lugares.db.LugarDAOMongo

class LugarController {
    private val lugarDB = LugarDAOMongo()
    fun getLugarInicial():Lugar?{
        return lugarDB.getById(100)
    }
    fun getLugar(lugar: String):Lugar?{
        return lugarDB.getByNombre(lugar)
    }
    fun getEnemigos(lugar: String):List<Enemigo>{
        return getLugar(lugar)?.enemigos ?: emptyList()
    }
    fun isOnLimits(lugar:Lugar, nuevo: Int):Boolean{
        lugar.limites.forEach {
            if(it == nuevo) return true
        }
        return false
    }
    fun getLimitesName(lugar:Lugar):List<String>{
        val list = mutableListOf<String>()
        lugarDB.getLimites(lugar).forEach {
            list.add(it.nombre)
        }
        return list
    }
}