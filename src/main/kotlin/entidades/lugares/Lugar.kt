package entidades.lugares

import entidades.enemigo.Enemigo
import kotlinx.serialization.Serializable

@Serializable
data class Lugar (val _id:Int, val nombre:String,val tipo:TipoLugar,val limites:List<Int>,val enemigos:List<Enemigo>)
