package entidades.mensaje

import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import java.time.LocalDateTime

@Serializable
data class Mensaje(val user:String, val mensaje:String, @Contextual val timestamp : LocalDateTime )