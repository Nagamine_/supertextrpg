package entidades.mensaje

interface MensajeDAO {
    fun getTodos(): Collection<Mensaje>
    fun getMensaje(id: Int): Mensaje?
}