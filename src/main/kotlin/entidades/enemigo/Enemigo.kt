package entidades.enemigo

import entidades.item.ItemIdCantidad
import entidades.stats.Stats
import kotlinx.serialization.Serializable

@Serializable
data class Enemigo(val nombre :String,val stats:Stats,val recompBase:Int,val recompItem:List<ItemIdCantidad>)