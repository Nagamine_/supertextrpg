package entidades.enemigo

import entidades.lugares.LugarController

class EnemigoController {
    private val lugarController = LugarController()
    fun getByLugar(lugar:String):List<Enemigo>{
        return lugarController.getEnemigos(lugar)
    }
}