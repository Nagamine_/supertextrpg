package entidades.item

import kotlinx.serialization.Serializable

@Serializable
data class InventarioItem(val item:Item, var cantidad:Int) {
}