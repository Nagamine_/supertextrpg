package entidades.item

import kotlinx.serialization.Serializable

@Serializable
data class Item(val _id:Int, val nombre:String, val descripcion:String)