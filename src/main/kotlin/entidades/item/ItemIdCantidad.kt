package entidades.item

import kotlinx.serialization.Serializable

@Serializable
data class ItemIdCantidad (val itemId:Int,var cantidad:Int)