package entidades.cuenta

import io.javalin.core.security.Role

enum class Rol : Role {
    TODOS,ADMIN, USER
}