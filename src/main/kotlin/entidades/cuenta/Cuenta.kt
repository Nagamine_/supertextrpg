package entidades.cuenta

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.litote.kmongo.id.MongoId
@Serializable
data class Cuenta(@SerialName("_id") @MongoId val usuario:String, val pass:String, val rol: Rol)