package entidades.cuenta

import at.favre.lib.crypto.bcrypt.BCrypt
import at.favre.lib.crypto.bcrypt.LongPasswordStrategies
import entidades.cuenta.db.CuentaDAOMongo


class CuentaController {
    val cuenta = CuentaDAOMongo()
    fun autenticar(usuario:String,contra:String):Boolean{
        if(cuenta.getCuenta (usuario)!=null){
            return BCrypt.verifyer(BCrypt.Version.VERSION_2Y, LongPasswordStrategies.hashSha512(BCrypt.Version.VERSION_2Y)).verify(contra.toCharArray(),cuenta.getCuenta(usuario)?.pass).verified

        }
        return false

    }
    fun existeUser(usuario: String):Boolean{
        return cuenta.getCuenta(usuario)!=null
    }
    fun getCuenta(usuario: String):Cuenta?{
        return cuenta.getCuenta(usuario)
    }
    fun añadir(usuario: String,pass:String,rol: Rol):Boolean{
        if(usuario != "" && pass != "") {
            val hash = BCrypt.with(BCrypt.Version.VERSION_2Y, LongPasswordStrategies.hashSha512(BCrypt.Version.VERSION_2Y)).hashToString(12, pass.toCharArray())
            return cuenta.addCuenta(Cuenta(usuario, hash, rol))
        }
        return false
    }
    fun delete(usuario: String):Boolean{
        return cuenta.delete(usuario)
    }
}