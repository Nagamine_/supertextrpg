package entidades.cuenta.db

import entidades.cuenta.Cuenta

interface CuentaDAO {
    fun getTodos():Collection<Cuenta>
    fun getCuenta(id:String): Cuenta?
    fun addCuenta(cuenta: Cuenta):Boolean
    fun delete(cuenta:String):Boolean
}