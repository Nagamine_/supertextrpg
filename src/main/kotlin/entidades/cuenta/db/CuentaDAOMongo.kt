package entidades.cuenta.db

import database.Mongo
import entidades.cuenta.Cuenta
import org.litote.kmongo.eq
import org.litote.kmongo.findOne
import org.litote.kmongo.getCollection

class CuentaDAOMongo: CuentaDAO {
    private val cuentaCol =  Mongo.getDatabase().getCollection<Cuenta>()
    override fun getTodos(): Collection<Cuenta> {
        val list = mutableListOf<Cuenta>()
        cuentaCol.find().toList()
        return list
    }

    override fun getCuenta(id: String): Cuenta? {
        return cuentaCol.findOne { Cuenta::usuario eq id }
    }

    override fun addCuenta(cuenta: Cuenta): Boolean {
        return cuentaCol.insertOne(cuenta).wasAcknowledged()
    }

    override fun delete(cuenta: String): Boolean {
        return cuentaCol.deleteOne(Cuenta::usuario eq cuenta).wasAcknowledged()
    }

}