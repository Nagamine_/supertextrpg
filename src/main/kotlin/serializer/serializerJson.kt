package serializer

import kotlinx.serialization.json.Json
import org.litote.kmongo.id.serialization.IdKotlinXSerializationModule

val serializerJson = Json { serializersModule = IdKotlinXSerializationModule }
