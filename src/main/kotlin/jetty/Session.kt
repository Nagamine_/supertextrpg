package jetty

import org.eclipse.jetty.server.session.DefaultSessionCache
import org.eclipse.jetty.server.session.FileSessionDataStore
import org.eclipse.jetty.server.session.SessionCache
import org.eclipse.jetty.server.session.SessionHandler
import java.io.File


class Session {
    fun fileSessionHandler(): SessionHandler {
        val sessionHandler = SessionHandler()
        val sessionCache: SessionCache = DefaultSessionCache(sessionHandler)
        sessionCache.sessionDataStore = fileSessionDataStore()
        sessionHandler.sessionCache = sessionCache
        sessionHandler.httpOnly = true
        // make additional changes to your SessionHandler here
        return sessionHandler
    }

    private fun fileSessionDataStore(): FileSessionDataStore {
        val fileSessionDataStore = FileSessionDataStore()

        val storeDir = File("/", "javalin-session-store")
        storeDir.mkdir()
        fileSessionDataStore.storeDir = storeDir
        return fileSessionDataStore
    }

}