package util

import kotlin.math.ceil
import kotlin.math.round
import kotlin.random.Random
import kotlin.random.asJavaRandom

fun randomNormalInt(prom:Int,varianza:Int):Int{
    return round(prom + Random.asJavaRandom().nextGaussian() * varianza).toInt()
}
fun randomNormal25Porciento(valor:Int):Int{
    return randomNormalInt(valor, ceil(valor*0.25).toInt())
}
fun List<String>.array2String(startIndex :Int= 0,endIndex:Int = this.size-1):String{
    var ed = endIndex
    if(endIndex >= this.size){
        ed = this.size-1
    }
    var s = ""
    for(i in startIndex..ed){
        s += this[i] + " "
    }
    return s.trim()
}