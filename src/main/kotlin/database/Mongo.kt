package database

import com.mongodb.client.MongoDatabase
import org.litote.kmongo.KMongo

object Mongo {
    private val client = KMongo.createClient()
    private val database = client.getDatabase("TxtRpg")
    fun getDatabase():MongoDatabase{
        return database
    }
}