package core


import entidades.cuenta.CuentaController
import entidades.cuenta.Rol
import io.javalin.http.Handler
import log.logger
import org.apache.commons.text.StringEscapeUtils.escapeHtml4


class LoginController {
    private val cuentaController = CuentaController()

    val log = Handler{ ctx ->

        if(ctx.basicAuthCredentialsExist()){
            val cred = ctx.basicAuthCredentials()
            if(cuentaController.autenticar(cred.username,cred.password)){
                ctx.sessionAttribute(Constantes.USUARIO,cred.username)
                ctx.status(200)
                logger().info("autenticado ${cred.username}")

            }else{
                ctx.status(401)
               ctx.result("Usuario o contraseña equivocados")
                logger().info("fallo en autentcar ${cred.username}")
            }
        }


    }
    val cerrarSesion = Handler { ctx ->
        val user =ctx.sessionAttribute<String>(Constantes.USUARIO)
        ctx.req.session.invalidate()
        ctx.status(201)
        logger().info("$user deslogueado")
    }
    val isOnline = Handler { ctx ->
        if(ctx.sessionAttribute<String>(Constantes.USUARIO)!=null){
            println(ctx.sessionAttribute<String>(Constantes.USUARIO))
            ctx.result("true")
        }else{
            println("bbbbbbbbbbbbbbbbbbbb")
            ctx.result("false")

        }

        ctx.status(201)
    }
    val register = Handler {ctx ->

        var user = ctx.basicAuthCredentials().username
        val pass = ctx.basicAuthCredentials().password
        val existe = cuentaController.existeUser(user) //cuentas.buscarUser(ctx.basicAuthCredentials().username) != null
        user = escapeHtml4(user)
        if(existe){
            logger().info("$user ya existe")
            ctx.status(400)
            ctx.result("$user ya existe")
        }else if(user != "" && pass != ""){
            if(cuentaController.añadir(user,pass, Rol.USER)){
                logger().info("registrado $user")
                ctx.result("$user registrado exitosamente")
                ctx.status(201)
            }else{
                logger().info("error al registrar $user")
                ctx.result("error al registrar $user")
                ctx.status(400)
            }
        }
    }
}