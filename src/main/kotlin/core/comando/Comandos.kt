package core.comando


object Comandos {

    private val comandos = listOf(
            Comando(TipoComando.MENSAJE,"mensaje","m").apply {
              necesitaPersonaje=false
                combate = CombateContext.TODOS
            },
            Comando(TipoComando.CREAR, "create", "c").apply {
                necesitaPersonaje = false
                combate = CombateContext.TODOS
            },
            Comando(TipoComando.TRABAJAR, "trabajar", "w"),
            Comando(TipoComando.BALANCE, "balance", "bal").apply {
                combate = CombateContext.TODOS
            },
            Comando(TipoComando.VIAJAR, "viajar", "v"),
            Comando(TipoComando.CAZAR, "cazar", "c"),
            Comando(TipoComando.ATACAR, "atacar").apply {
                combate = CombateContext.DENTRO
            },
            Comando(TipoComando.HUIR,"huir","escapar").apply {
                combate = CombateContext.DENTRO
            },
            Comando(TipoComando.STATS,"stats").apply {
                combate = CombateContext.DENTRO
            },
            Comando(TipoComando.INFO,"info").apply {
                combate = CombateContext.TODOS
            },
            Comando(TipoComando.INVENTARIO, "inventario","i").apply {
                combate = CombateContext.TODOS
            }

    )

    fun getComando(comando: String): Comando? {
        comandos.forEach {
            if (it.comandos.contains(comando)) {
                return it
            }
        }
        return null
    }
}