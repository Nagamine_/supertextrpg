package core.comando

class Comando (val tipoComando: TipoComando, vararg val comandos:String){

    var necesitaPersonaje:Boolean = true
    var combate: CombateContext = CombateContext.FUERA
}
enum class CombateContext{
    TODOS,DENTRO,FUERA
}
enum class TipoComando {
    MENSAJE,CREAR, TRABAJAR, BALANCE, VIAJAR, CAZAR, ATACAR,STATS, HUIR, INFO,INVENTARIO,
}