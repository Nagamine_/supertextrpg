package core.comando

import core.batalla.EntityBatalla
import core.batalla.SalaBatalla
import entidades.enemigo.Enemigo
import entidades.personaje.Personaje
import org.litote.kmongo.Id
import java.util.concurrent.ConcurrentHashMap

object GamuController {
    private val salaBatallas = ConcurrentHashMap<Id<Personaje>, Id<SalaBatalla>>()
    private val batallas = ConcurrentHashMap<Id<SalaBatalla>, SalaBatalla>()
    fun crearSalaSimple(personaje: Personaje,enemigo:Enemigo){
        val p = EntityBatalla(1,personaje)
        val e = EntityBatalla(2,enemigo)
        val sala = SalaBatalla(p,e)
        salaBatallas[personaje._id] = sala.id
        batallas[sala.id] = sala
    }
    fun isEnCombate(personaje: Id<Personaje>):Boolean{
        return salaBatallas[personaje]!=null
    }
    fun getSala(personaje: Id<Personaje>):SalaBatalla{
        return batallas[salaBatallas[personaje]]!!
    }
    fun endBatalla(personaje: Id<Personaje>){
        batallas.remove(salaBatallas[personaje])
        salaBatallas.remove(personaje)
    }

}