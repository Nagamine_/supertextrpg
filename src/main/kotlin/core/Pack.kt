package core

import kotlinx.serialization.Serializable

@Serializable
data class Pack(val tipo:String,val body:String)