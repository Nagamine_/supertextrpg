package core.batalla

import entidades.stats.Stats
import org.litote.kmongo.Id
import org.litote.kmongo.newId

class SalaBatalla(vararg val entities :EntityBatalla){
    val id: Id<SalaBatalla> = newId()
    val entityBatallaController = EntityBatallaController()
    fun atacar(origen:Id<EntityBatalla>,objetivo:Id<EntityBatalla>):Int{
        val o =entities.filter { it.id == origen }.first()
        val d = entities.filter { it.id == objetivo }.first()
        return entityBatallaController.daño(o,d,15)
    }
    private var contador = 0
    fun avanzarTurno(){
        if(contador>=entities.size){
            contador = 0
        }else{
            contador++
        }
    }
    fun getSiguienteTurno():EntityBatalla{
        return entities[contador]
    }
    fun getStats(entityBatalla: Id<EntityBatalla>): Stats {
       return  entities.filter { it.id==entityBatalla }.first().statsActuales
    }
    fun getEntidad(entityBatalla: Id<EntityBatalla>):EntityBatalla{
        return  entities.filter { it.id==entityBatalla }.first()
    }
    fun getEnemigos(entityBatalla: Id<EntityBatalla>):List<EntityBatalla>{
        val bando = entities.filter { it.id == entityBatalla }.first().bando
        return entities.filter { it.bando != bando }
    }

}