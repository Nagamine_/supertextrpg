package core.batalla

import entidades.enemigo.Enemigo
import entidades.personaje.Personaje
import entidades.stats.Stats
import org.litote.kmongo.Id
import org.litote.kmongo.newId

class EntityBatalla {
    val bando:Int
    val nombre:String
    val statsBase:Stats
    val statsActuales :Stats
    val id: Id<EntityBatalla>
    var recompensa = 0
    constructor(bando:Int,enemigo:Enemigo){
        this.bando = bando
        nombre = enemigo.nombre
        statsBase = enemigo.stats
        statsActuales = enemigo.stats.copy()
        recompensa = enemigo.recompBase
        id = newId()
    }
    constructor(bando:Int, personaje:Personaje){
        this.bando = bando
        nombre = personaje.nombre
        statsBase = personaje.data.stats
        statsActuales = personaje.data.statsActuales
        id=personaje._id.cast()
    }

}