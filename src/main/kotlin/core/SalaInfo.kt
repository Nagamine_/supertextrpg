package core

import entidades.personaje.Personaje
import kotlinx.serialization.Serializable

@Serializable
class SalaInfo {
    val usuario :String
    val personaje :String
    val lugar :String
    constructor(usuario:String,per:String,lugar:String){
        this.usuario = usuario
        this.personaje=per
        this.lugar=lugar
    }
    constructor(per: Personaje) {
        usuario=per.cuenta
        personaje=per.nombre
        lugar=per.lugar.nombre
    }
}