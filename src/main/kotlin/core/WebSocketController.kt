package core

import core.batalla.EntityBatalla
import core.comando.*
import entidades.lugares.LugarController
import entidades.lugares.TipoLugar
import entidades.mensaje.Mensaje
import entidades.personaje.Personaje
import entidades.personaje.PersonajeController
import io.javalin.websocket.WsContext
import io.javalin.websocket.WsHandler
import io.javalin.websocket.WsMessageContext
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import log.logger
import org.litote.kmongo.json
import util.array2String
import java.time.LocalDateTime
import java.util.concurrent.ConcurrentHashMap
import javax.naming.OperationNotSupportedException
import kotlin.random.Random


class WebSocketController {
    private val userUsernameMap = ConcurrentHashMap<WsContext, String>()
    private val personajeController = PersonajeController()
    private val lugarController = LugarController()

    private val onlinePersonajes = ConcurrentHashMap<String, PersonajeInstancias>() //usuario - personaje
    fun gamusocket(ws: WsHandler) {
        ws.onConnect { ctx ->
            val usuario = ctx.sessionAttribute<String>(Constantes.USUARIO)
            logger().info("conectado: $usuario ")
            if (usuario != null) {
                userUsernameMap[ctx] = usuario
                broadcastMessage(Constantes.NAMESERVER, "$usuario se acaba de conectar")
                val p = personajeController.buscarPersonajeFromCuenta(usuario)
                if (p != null) {
                    addOnlinePersonaje(usuario, p)

                } else {
                    broadcastMessage(Constantes.NAMESERVER, "Parece que aun no creas tu personaje, usa el comando !create <nombre> para crear uno")
                }
                sendInfo()
            } else {
                logger().error("userid es null")
            }
        }
        ws.onClose { ctx ->
            val username = userUsernameMap[ctx]
            userUsernameMap.remove(ctx)
            onlinePersonajes[username]?.let { par ->
                if (par.instancias > 1) {
                    par.instancias--
                } else {
                    onlinePersonajes.remove(username!!)
                }
            }
            broadcastMessage(Constantes.NAMESERVER, "$username abandono el chat")
        }
        ws.onMessage { ctx ->
            comando(ctx)
        }
    }

    fun addOnlinePersonaje(usuario: String, personaje: Personaje) {
        val i = onlinePersonajes[usuario]
        if (i != null) {
            i.instancias++
        } else {
            onlinePersonajes[usuario] = PersonajeInstancias(1, personaje)
        }
    }

    fun comando(ctx: WsMessageContext) {
        //println(ctx.message())
        //logger().info(ctx.message())
        val com = Json.decodeFromString<Pack>(ctx.message())
        // logger().info(com.toString())
        val cuenta = userUsernameMap[ctx]!!
        broadcastMessage(cuenta,  com.body)
        val arg = procesarBody(com.body)
        val p = onlinePersonajes[cuenta]?.personaje
        checkComando(com.tipo, cuenta){comando ->
            when (comando) {
                TipoComando.CREAR -> crear(cuenta,arg)
                TipoComando.TRABAJAR -> trabajar(p!!)
                TipoComando.BALANCE -> balance(p!!)
                TipoComando.VIAJAR -> viajar(p!!, arg)
                TipoComando.CAZAR -> cazar(p!!)
                TipoComando.ATACAR -> atacar(p!!)
                TipoComando.HUIR -> huir(p!!)
                TipoComando.STATS ->stats(p!!)
                TipoComando.INFO -> infoPersonaje(p!!)
                TipoComando.INVENTARIO -> inventario(p!!)
            }
            sendInfo()
        }
    }
    fun checkComando(comando: String, cuenta: String, accion: (comando: TipoComando) -> Unit) {
        var res = true
        checkNotNull(comando) { com ->
            if (com.necesitaPersonaje) {
                checkPersonaje(cuenta, com) { comando, personajeStatus ->
                    res = true
                    if(res) res = checkCombate(comando,personajeStatus)
                }
            }
            if(res) accion(com.tipoComando)
        }
    }

    fun checkNotNull(comando: String, accion: (c: Comando) -> Unit) {
        val c = Comandos.getComando(comando)
        if (c != null) {
            accion(c)
        } else {
            broadcastMessage("System", "El comando $comando no existe")

        }
    }

    fun checkCombate(c: Comando, p: Personaje): Boolean {
        val enCombate = GamuController.isEnCombate(p._id)
         when (c.combate) {
            CombateContext.TODOS -> return true
            CombateContext.DENTRO -> {
                if(enCombate){
                    return true
                }else{
                    broadcastMessage("System", "Solo puedes realizar esta accion en combate")
                }
            }
            CombateContext.FUERA -> {
                if(!enCombate){
                    return true
                }else{
                    broadcastMessage("System", "Solo puedes realizar esta accion fuera de combate")
                }
            }
        }
        return false
    }


    fun checkPersonaje(cuenta: String, comando: Comando, accion: (c: Comando, p: Personaje) -> Unit) {
        val p = onlinePersonajes[cuenta]
        if (p != null) {
            accion(comando, p.personaje
            )
        } else {
            broadcastMessage(Constantes.NAMESERVER, "$cuenta parece que aun no creas tu personaje, usa el comando !create <nombre> para crear uno")
        }
    }
    fun procesarBody(body: String): List<String> {

        return if(body!=""){
            body.subSequence(1, body.length).split(" ")
        }else{
            emptyList()
        }
    }


    fun crear(cuenta: String, nombre: List<String>) {
        val p = personajeController.buscarPersonajeFromCuenta(cuenta)
        if (p != null) {
            broadcastMessage(Constantes.NAMESERVER, "$cuenta  parece que ya tienes un personaje creado, por el momento no puedes crear otro")
        } else {
            if (nombre.size > 1) {
                val name = nombre.array2String(1)
                val per = personajeController.crearPersonaje(name, cuenta)
                if (per != null) {
                    broadcastMessage(Constantes.NAMESERVER, "Bienvenido $name")
                    addOnlinePersonaje(cuenta, per)
                } else {
                    broadcastMessage(Constantes.NAMESERVER, "error al crear el personaje $name")
                }
            } else {
                broadcastMessage(Constantes.NAMESERVER, "Debes poner el comando junto al nombre ex: !create chutulu")
            }
        }
    }

    fun viajar(personaje: Personaje, body: List<String>) {

        if (body.size > 1) {
            val name = body.array2String(1)
            val l = lugarController.getLugar(name)
            if (l != null) {
                if (lugarController.isOnLimits(personaje.lugar,l._id) ) {
                    if (personajeController.viajar(personaje, l._id)) {
                        broadcastMessage(Constantes.NAMESERVER, "${personaje.nombre} ha viajado a ${l.nombre}")
                    } else {
                        broadcastMessage(Constantes.NAMESERVER, "El viaje fallo")
                    }
                } else {
                    broadcastMessage(Constantes.NAMESERVER, "$name esta muy lejos")
                }
            } else {
                broadcastMessage(Constantes.NAMESERVER, "$name no existe")
            }

        } else {
            broadcastMessage(Constantes.NAMESERVER, "los destinos a los que puedes viajar son: ${lugarController.getLimitesName(personaje.lugar)}")
        }
    }

    fun trabajar(personaje: Personaje) {
        val oro = personajeController.trabajar(personaje)
        broadcastMessage(Constantes.NAMESERVER, "${personaje.nombre} trabajo y gano $oro")
    }

    fun balance(personaje: Personaje) {
        broadcastMessage(Constantes.NAMESERVER, "${personaje.nombre} tiene ${personaje.data.oro}")
    }

    fun cazar(personaje: Personaje) {
        if (personaje.lugar.tipo == TipoLugar.CAZA) {
            val e = personaje.lugar.enemigos.random()
            GamuController.crearSalaSimple(personaje,e)
            broadcastMessage(Constantes.NAMESERVER, "Te encuentras un ${e.nombre}<br>" +
                    "${e.stats}")
        } else {
            broadcastMessage(Constantes.NAMESERVER, "No puedes cazar aqui")
        }
    }
    fun atacar(personaje: Personaje){
        val sala = GamuController.getSala(personaje._id)
        val enem = sala.getEnemigos(personaje._id.cast())
        if(enem.size == 1){
            val e =enem.first()
            var daño= sala.atacar(personaje._id.cast(),e.id)
            broadcastMessage(Constantes.NAMESERVER, "Atacas a ${e.nombre} haciendole $daño de daño<br>" +
                    "${e.nombre} ${e.statsActuales}")
            daño = sala.atacar(e.id,personaje._id.cast())
            if(checkMuerteEnemigo(e)){
                var mens = "${e.nombre} Ha muerto<br>"

                val rec = personajeController.getRecompensaOro(personaje,e.recompensa)
                val (nivel,xp) = personajeController.getRecompensaExp(personaje,e.recompensa)
                mens += "Ganaste $rec de oro y $xp puntos de experiencia<br>"
                if(nivel){
                    mens += "Subiste al nivel ${personaje.data.nivel}"
                }
                broadcastMessage(Constantes.NAMESERVER, mens)
                personajeController.reestablecer(personaje)
                personajeController.updateData(personaje)
                GamuController.endBatalla(personaje._id)
            }else{
                broadcastMessage(Constantes.NAMESERVER, "${e.nombre} te ataca haciendote $daño de daño<br>" +
                        "${personaje.nombre} ${sala.getStats(personaje._id.cast())}")
                checkMuertePersonaje(personaje,e)
            }

        }else{
            logger().error("mas de un enemigo")
            throw OperationNotSupportedException("mas de un enemigo")
        }
    }
    fun checkMuerteEnemigo(enemigo:EntityBatalla):Boolean{
        if(enemigo.statsActuales.vida <= 0){
            return true
        }
        return false
    }
    fun checkMuertePersonaje(personaje: Personaje, enemigo: EntityBatalla){
        val sala =GamuController.getSala(personaje._id)

        val stats = sala.getStats(personaje._id.cast())
        if(stats.vida <= 0){
            var s = "${personaje.nombre} Ha sido asesinado por ${enemigo.nombre}"
            personajeController.viajar(personaje,100)
            personajeController.reestablecer(personaje,vida = true)
            val rec = personajeController.getRecompensaOro(personaje,enemigo.recompensa/2*-1)
            s+= " y perdio ${rec*-1} de oro, su cuerpo será teletransportado a la ciudad"
            personajeController.updateData(personaje)
            broadcastMessage(Constantes.NAMESERVER,s)
            GamuController.endBatalla(personaje._id)
        }

    }
    fun huir(personaje: Personaje){
        if(Random.nextFloat()>0.5){
            broadcastMessage(Constantes.NAMESERVER, "${personaje.nombre} Lograste huir")
            GamuController.endBatalla(personaje._id)
        }else{
            broadcastMessage(Constantes.NAMESERVER, "${personaje.nombre} Intentaste huir, pero te atacaron")
        }
    }
    fun stats(personaje: Personaje){
        val sala = GamuController.getSala(personaje._id)
        var s = ""
        sala.entities.forEach {
            s += "<b>${it.nombre}</b>\nStats base:${it.statsBase}\nStats actuales:${it.statsActuales}\n\n"
        }
        sendStats(Constantes.NAMESERVER,s)
    }
    fun infoPersonaje(personaje: Personaje){
        sendJson(Pack("infoPersonaje", personaje.json))
        //sendStats(Constantes.NAMESERVER,"nombre: ${personaje.nombre}<br>${personaje.data} <br>ubicacion:${personaje.lugar.nombre}")
    }
    fun inventario(personaje: Personaje){
        var s = ""
        personaje.inventario.forEach {
            s += "id:${it.item._id} nombre:${it.item.nombre} cantidad:x${it.cantidad} \n"
            s += "descripcion:${it.item.descripcion}\n\n"
        }
        broadcastMessage(Constantes.NAMESERVER, s)
    }
    fun sendInfo() {
        val list = mutableListOf<SalaInfo>()
        val setUsers = mutableSetOf<String>()//para evitar duplicados
        userUsernameMap.values.forEach {
            if (!setUsers.contains(it)) {
                setUsers.add(it)
                val p = onlinePersonajes[it]
                if (p != null) {
                    list.add(SalaInfo(p.personaje))
                } else {
                    list.add(SalaInfo(it, "", ""))
                }
            }

        }
        sendJson(Pack("info", list.json))
    }
    fun sendStats(sender: String, message: String){
        val mensaje = Mensaje(sender, message, LocalDateTime.now())
        sendJson(Pack("stats", mensaje.json))
    }
    fun broadcastMessage(sender: String, message: String) {
        var m = message
        if (m.length > Constantes.MAXCHARACTERS) {
            m = m.substring(0, Constantes.MAXCHARACTERS - 1)
        }
        val mensaje = Mensaje(sender, m, LocalDateTime.now())
        println(mensaje)
        if(mensaje.mensaje!=""){
            sendJson(Pack("mensaje", mensaje.json))
        }

    }

    fun sendJson(pack: Pack) {
        userUsernameMap.keys.filter { it.session.isOpen }.forEach { session ->
            session.send(pack.json)
        }
    }
}