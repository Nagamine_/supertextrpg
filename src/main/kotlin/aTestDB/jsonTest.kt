package aTestDB

import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonObject
import org.litote.kmongo.json

fun main(){
    val j =mapOf(
            "name" to "ilkin",
            "age" to 37,
            "male" to true,
            "contact" to arrayOf(
                    "city" to "istanbul",
                    "email" to "xxx@yyy.com"
            )
    )
    JsonObject
    println(j)
    print(j.json)
}