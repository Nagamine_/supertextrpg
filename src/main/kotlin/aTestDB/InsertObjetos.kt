package aTestDB

import entidades.item.Item
import org.litote.kmongo.KMongo
import org.litote.kmongo.getCollection

fun main(){
    val client = KMongo.createClient()
    val database = client.getDatabase("TxtRpg")

    val col = database.getCollection<Item>()
    col.insertMany(listOf(
            Item(100,"Engranaje","Un engranaje,puede servir para crear cosas"),
            Item(101,"Acero","Acero solido, tiene una gran dureza y es perfecto para crear armas y armaduras")
    ))
}