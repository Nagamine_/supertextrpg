package aTestDB


import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.bson.Document
import org.litote.kmongo.*

@Serializable
data class Prub(val nombre:String, val a:String)
@Serializable
data class PrubInPrub(val nombre:String,@Contextual val prub :Prub)
fun main(){
    println("a")
    val client = KMongo.createClient()
    val database = client.getDatabase("TxtRpg")

    val col = database.getCollection<Prub>()
    val c = Document("_id","xdgd").append("a","z")
    //database.getCollection("prub").insertOne(c)
    val d = col.aggregate<Prub>(project(Prub::nombre from "\$_id",Prub::a from Prub::a)).toList()
    //val d = col.aggregate<Prub>("[{\$project:{_id:0,nombre:'\$_id',a:1}}]").toList()
    println(d)
    println(Json.encodeToString(d))
    //col.insertOne(c)
    val col2 = database.getCollection<PrubInPrub>()
    val f = Document("nombre","asd").append("prub","xdgd")
    //database.getCollection("prubInPrub").insertOne(f)
    val g =col2.aggregate<PrubInPrub>(lookup("prub", emptyList(),PrubInPrub::prub ,project("_id" from 0,Prub::nombre from "\$_id",Prub::a from Prub::a)), unwind("\$prub")).toList()
    g.forEach { println(it) }
    //val r = col2.findOne(PrubInPrub::nombre eq "k3")
    //println(r)
}