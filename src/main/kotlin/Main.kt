import core.Constantes
import core.LoginController
import core.WebSocketController
import entidades.cuenta.CuentaController
import entidades.cuenta.Rol
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.core.security.SecurityUtil.roles
import io.javalin.http.Handler
import log.logger


fun main(args: Array<String>) {
    val app = Javalin.create(){
        it.enableCorsForOrigin("http://localhost","https://nagamine.serverless.social")
    }
    val webSocket = WebSocketController()
    val loginController = LoginController()
    val cuentaController  = CuentaController()

    app.config.apply {

      //  sessionHandler { s.fileSessionHandler() }
        accessManager {handler, ctx, permittedRoles ->
            var role =Rol.TODOS
            ctx.sessionAttribute<String>(Constantes.USUARIO)?.let{ username ->
                 role = cuentaController.getCuenta(username)?.rol ?: Rol.TODOS
            }

            if(permittedRoles.contains(Rol.TODOS)){
                handler.handle(ctx)
            }else{
                if(role == Rol.ADMIN){
                    handler.handle(ctx)
                }else if(permittedRoles.contains(role)){
                    handler.handle(ctx)
                }else{
                    logger().error("Sin permiso")
                    ctx.status(401).result("Unauthorized")
                }
            }


        }

    }
    val gett = Handler{ ctx ->
      // println(ListCuenta().getTodos().toString())
        ctx.result("holi")
    }

    app.routes {
        post("/login",loginController.log,roles(Rol.TODOS))
        post("/register",loginController.register,roles(Rol.TODOS))
        ws("/test", webSocket::gamusocket,roles(Rol.USER))
        get("/get", gett,roles(Rol.TODOS))
        get("/online", loginController.isOnline,roles(Rol.TODOS))
        get("/logout",loginController.cerrarSesion,roles(Rol.TODOS))
    }


    app.start(7000)


}


