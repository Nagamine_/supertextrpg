import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import util.array2String
import util.randomNormal25Porciento
import util.randomNormalInt
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class Util {
    val tamanoLista = 100
    @ParameterizedTest
    @CsvSource(
            "1,  10",
            "50,1",
            "300, 40",
            "20,5000",
            "350 ,150"
    )
    fun testRandomEntero(prom:Int,varianza:Int){

        val list = mutableListOf<Int>()
        for(i in 1..tamanoLista){
            list.add(randomNormalInt(prom,varianza))
        }
        println(list)
        var prom = 0.0
        list.forEach {
            prom += it
        }
        prom /= list.size
        println("el promedio es $prom")
        var desviacionEstandar = 0.0
        list.forEach {
            desviacionEstandar += (it-prom).pow(2)
        }
        assertEquals(tamanoLista,list.size)
        desviacionEstandar /= list.size
        desviacionEstandar = sqrt(desviacionEstandar)
        println(" la desviacion estandar es $desviacionEstandar")

    }
    @ParameterizedTest
    @CsvSource(
            "1",
            "50",
            "300",
            "5000",
            "-50",
            "0"
    )
    fun testRandom25Porciento(prom:Int){

        val list = mutableListOf<Int>()
        for(i in 1..tamanoLista){
            list.add(randomNormal25Porciento(prom))
        }
        println(list)
        var prom = 0.0
        list.forEach {
            prom += it
        }
        prom /= list.size
        println("el promedio es $prom")
        var desviacionEstandar = 0.0
        list.forEach {
            desviacionEstandar += (it-prom).pow(2)
        }
        assertEquals(tamanoLista,list.size)
        desviacionEstandar /= list.size
        desviacionEstandar = sqrt(desviacionEstandar)
        println(" la desviacion estandar es $desviacionEstandar")

    }
    @Test
    fun testArray2String(){
        val arr = listOf("a","b","c")
        var s = arr.array2String()
        assertEquals("a b c",s)
        s = arr.array2String(1)
        assertEquals("b c",s)
        s = arr.array2String(endIndex = 1)
        assertEquals("a b",s)
        s=arr.array2String(1,2)
        assertEquals("b c",s)
    }
}