import entidades.cuenta.CuentaController
import entidades.cuenta.Rol
import entidades.lugares.Lugar
import entidades.personaje.Personaje
import entidades.personaje.PersonajeController
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals
import kotlin.test.assertTrue
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class Personaje {
    val a = PersonajeController()
    val c = CuentaController()

    lateinit var personaje : Personaje
    // val personaje :Personaje
    @BeforeAll
    fun initdb(){
        c.añadir("testCuenta","cuenta", Rol.ADMIN)
        a.crearPersonaje("test","testCuenta")
        personaje = a.buscarPersonajeFromCuenta("testCuenta")!!
    }

    @Test
    fun trabajoTest(){
        var oro = personaje.data.oro
        oro += a.trabajar(personaje)
        println(oro)
        assertEquals(oro,personaje.data.oro)
    }
    @Test
    fun viajeTest(){
        val destino=personaje.lugar.limites[0]
        val r =a.viajar(personaje,destino)
        assertTrue(r)
    }
    @AfterAll
    fun eliminar(){
        a.borrar(personaje)
        c.delete("testCuenta")
    }
}