import entidades.personaje.PersonajeController
import entidades.personaje.db.PersonajeDAOMongo
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class bd {
    private val per = PersonajeController()
    private val ppp = PersonajeDAOMongo()
    @Test
    fun buscarPersonajeFromCuenta(){
        val p =per.buscarPersonajeFromCuenta("k")
        println(p)
        assertEquals("k",p?.cuenta)
    }
    @Test
    fun getTodos(){
        val p = ppp.getTodos()
        println(p)
    }
}