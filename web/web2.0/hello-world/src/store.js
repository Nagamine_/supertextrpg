import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    data: []
  },
  mutations: {
      obtenerData(state,post){
          state.data = post
      }
  },
  actions:{
      getData: async function({commit}){
          await fetch('https://jsonplaceholder.typicode.com/posts/')
              .then(response => {
                  return  response.json();
              }).then(json =>{
                  commit('obtenerData',json)
                  console.log('obtenerData',json);
              })
              .catch(error => {
                  console.log('-----error-------');
                  console.log(error);
              })
      }
  }
})
