// small helper function for selecting element by id
let id = id => document.getElementById(id);
let ws;
let timeout = 100
let starTime;
const url = "localhost:7000"
const urlREST = "http://localhost:7000";
//const url = "nagamineapi.serverless.social"


const tx = document.getElementsByTagName('textarea');
for (let i = 0; i < tx.length; i++) {
    tx[i].setAttribute('style', 'height:' + (tx[i].scrollHeight) + 'px;resize:none;');//overflow-y:hidden;
    tx[i].addEventListener("input", OnInput, false);
}

function OnInput() {
    this.style.height = 'auto';
    if (this.scrollHeight <= 85) {
        this.style.height = (this.scrollHeight) + 'px';
    }else{
        this.style.height = 85 + 'px';
    }
}

const logout = async () => {
    const response = await fetch(urlREST + "/logout", {
            //withCredentials: true,
            credentials: 'include'
        }).then(respons => {
            if (!respons.ok) {
                throw Error(respons.statusText);
            } else {
                ws.close()
            }
            return respons.text();
        }).then(json => {
            console.log(json);
        })
        .catch(function(err) {
            console.log(err)
            //$("#errorapi").removeClass('d-none');
            console.log("error");
        });
}
id("logout").addEventListener("click", logout);

function connect() {

    ws = new WebSocket("ws://" + url + "/test");
    console.log("WebSocket connection open");
    ws.onopen = () => {
        let starTime = performance.now()
        console.log(starTime)
        if (starTime - e > 100) {
            timeout = 100
        } else {
            console.log("reset")
        }

    }
    //Establish the WebSocket connection and set up event handlers
    ws.onmessage = msg => comando(msg.data);
    ws.onclose = () => {
        console.log("WebSocket connection closed" + timeout);
        setTimeout(function() {
            connect();
            e = performance.now()
            console.log(e)
        }, Math.min(10000, timeout += timeout));

    };
    ws.onerror = err => {
        console.log(err)
        window.location.replace("/")
    }
};
connect();

// Add event listeners to button and input field
id("send").addEventListener("click", () => sendAndClear(id("message").value));
id("message").addEventListener("keypress", function(e) {
    if (e.keyCode == 13 && e.shiftKey) {

    } else
    if (e.keyCode === 13) { // Send message if enter is pressed in input field
        e.preventDefault();
        sendAndClear(e.target.value);
    }
});

function sendAndClear(message) {
    let m = message.trim()
    let pack = {
        tipo: "",
        body: ""
    }
    if (m !== "") {
        if (m.charAt(0) === '!') {
            let comm = m.substring(1).split(" ");
            pack.tipo = comm[0];
            pack.body = m;
        } else {
            pack.tipo = "mensaje";
            pack.body = m;
        }
        id("message").value = "";
        id("message").dispatchEvent(new Event("input"));
        ws.send(JSON.stringify(pack));
    }
}

function comando(msg) {

    let data = JSON.parse(msg)
    switch (data.tipo) {
        case "mensaje":
            updateChat(JSON.parse(data.body))
            break;
        case "stats":
            updateChatStats(JSON.parse(data.body))
            break;
        case "info":
            updateInfo(JSON.parse(data.body))
            break;
        default:

    }
}

function updateInfo(data) {
    console.log(data);
    id("userlist").innerHTML = data.map(info => {
        info = JSON.parse(info)
        return "<li>" + info.usuario + "[" + info.personaje + "] - " + info.lugar + "</li>"
    }).join("");

}

function updateChatStats(data) {
    console.log(JSON.stringify(data))
    let mensaje = `<article>
    <b>${data.user} dice:</b>
    <span class="timestamp">${new Date(data.timestamp.$date).toLocaleString()}</span>
    <div id="stats"><p>${data.mensaje}</p></div>
    </article>`
    id("chat").insertAdjacentHTML("afterbegin", mensaje);
}

function updateChat(data) { // Update chat-panel and list of connected users
    console.log(data)
    let mensaje = `<article>
    <b>${data.user} dice:</b>
    <span class="timestamp">${new Date(data.timestamp.$date).toLocaleString()}</span>
    <p>${data.mensaje}</p>
    </article>`
    id("chat").insertAdjacentHTML("afterbegin", mensaje);

}
