import Vue from "vue";
import Vuex from "vuex";
import login from "./modules/login.js"
import websocket from "./modules/websocket.js"
import chat from "./modules/chat.js"
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        url: "http://localhost:7000",
        bus: new Vue()
    },
    mutations: {

    },
    actions: {


    },
    getters:{
        bus: state=>state.bus
    },
    modules: {
        login,
        websocket,
        chat
    }
});
