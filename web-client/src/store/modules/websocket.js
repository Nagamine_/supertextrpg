/* eslint no-console: ["error", { allow: ["warn", "error","log"] }] */
import router from '../../router/index.js'
import ReconnectingWebSocket from 'reconnecting-websocket';
const websocket = {
    state: () => ({
        websocketURL: "ws://localhost:7000/test",
        websocket: null,
    }),
    mutations: {
        setwebsocket(state, websocket) {
            state.websocket = websocket
        },
    },
    actions: {
        connect: async function({ state,commit,rootState,dispatch}) {
            if(state.websocket === null){
                let ws = new ReconnectingWebSocket(state.websocketURL, [], {  minReconnectionDelay: 200,reconnectionDelayGrowFactor:1.5});
                ws.onopen = () => {

                    console.log("WebSocket connection open");

                }
                //Establish the WebSocket connection and set up event handlers
                ws.onmessage = msg =>{
                    let data = JSON.parse(msg.data)
                    rootState.bus.$emit(data.tipo,JSON.parse(data.body));
                }
                ws.onclose = () => {
                    console.log("WebSocket connection closed");

                };
                ws.onerror = err => {
                    console.log(err)
                    dispatch('logout')
                    router.push("/login")
                }
                commit('setwebsocket',ws)
            }else{
                    dispatch('reconnect')

            }

        },
        disconnect:async function({state}){
                if(state.websocket!==null){
                    await state.websocket.close()
                     state.websocket = null
                }
        },
        reconnect:async function({state,dispatch}){
            if(state.websocket===null){
                dispatch('connect')
            }else{
                if(state.websocket.readyState !== 1){
                    await state.websocket.reconnect()
                }
            }

        }
    },
    getters: {
        websocketURL: state => state.websocketURL,
        websocket: state => state.websocket,
        iswebsocketnull: state => state.websocket === null
    }
}
export default websocket;
