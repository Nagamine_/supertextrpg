/* eslint no-console: ["error", { allow: ["warn", "error"] }] */
const login = {
    state: () => ({
        logged: false,
        respuesta: ""
    }),
    mutations: {
        log(state) {
            state.logged = true
        },
        logout(state) {
            state.logged = false
        },
        respuesta(state, respuesta) {
            state.respuesta = respuesta
        }
    },
    actions: {
        login: async function({commit,dispatch,rootState}, form) {
            const basicAuth = 'Basic ' + btoa(form.username + ':' + form.password);
            let myHeaders = new Headers();
            myHeaders.append('Authorization', basicAuth);
            myHeaders.append('Content-Type', 'Authorization');
            await fetch(rootState.url + "/login", {
                    method: 'POST',
                    credentials: 'include',
                    //withCredentials: true,
                    headers: myHeaders,
                }).then(response => {
                    if (response.ok) {
                        commit('log')
                        dispatch('connect')
                    }
                    return response.text()
                }).then(text =>{
                    commit('respuesta',text)
                })
                .catch(err => {
                    console.error(err)
                });

        },
        logout: async function({commit, dispatch,rootState}) {
            await fetch(rootState.url + "/logout", {
                    //withCredentials: true,
                    credentials: 'include'
                }).then(respons => {
                    if (!respons.ok) {
                        throw Error(respons.statusText);
                    } else {
                        commit('logout')
                        dispatch('disconnect')
                        this.$route.push("/login")
                        //    ws.close()
                    }
                    return respons.text();
                }).then(json => {
                    commit('respuesta',json)
                    console.warn(json);
                })
                .catch(function(err) {
                    console.warn(err)
                    //$("#errorapi").removeClass('d-none');
                    console.error("error");
                });
        },
        checklogged: async function({commit,dispatch,rootState}){
            await fetch(rootState.url + "/online", {
					//withCredentials: true,
					credentials: 'include'
				})
				.then(function(response) {
					if (!response.ok) {
						throw Error(response.statusText);
					}
					return response.text();
				}).then(json => {
					if (json=="true") {
						commit('log')
                        dispatch('connect')
					}
				})
				.catch(function(err) {
					//$("#errorapi").removeClass('d-none');
					console.error(err);
				});
        },
        register: async function({commit,rootState},form){
            let username = form.username
            let password = form.password
            if (username !== "" && password !== "") {
                const basicAuth = 'Basic ' + btoa(username + ':' + password);
                let myHeaders = new Headers();
                myHeaders.append('Authorization', basicAuth);
                myHeaders.append('Content-Type', 'Authorization');
                await fetch(rootState.url + "/register", {
                        method: 'POST',
                        credentials: 'include',
                        //withCredentials: true,
                        headers: myHeaders,
                    }).then(response => {
                        return response.text();
                    })
                    .then(json => {
                        commit('respuesta',json)
                    }).catch(err => {
                        console.error(err)
                    });
            }
        }
    },
    getters: {
        logged: state => state.logged,
        respuesta: state => state.respuesta
    }
}
export default login;
