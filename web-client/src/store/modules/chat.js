const chat ={
    state: () => ({
        chatInfo: []    ,
        chatMessages: [],
    }),
    mutations: {
        addInfo(state,info) {
            state.chatInfo = info
        },
        addMensaje(state,msg) {
            state.chatMessages.unshift(msg)
        },
    },
    actions:{
        
    },
    getters:{
        chatInfo: state => state.chatInfo,
        chatMessages: state => state.chatMessages
    }

}
export default chat
