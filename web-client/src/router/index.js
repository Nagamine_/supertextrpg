import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/game/Home.vue";
import Secure from "../views/game/Secure.vue"
import Login from "../views/Login.vue"
import store from "../store/index.js"
import Chat from "../views/game/Chat.vue"
import Game from "../views/Game.vue"
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login
  },
  {
      path: '/login',
      name: 'login',
      component: Login
    },
  {
    path: '/game',
    component: Game,
    children: [
        {
          path: "about",
          name: "About",
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () =>
            import(/* webpackChunkName: "about" */ "../views/game/About.vue")
        },
        {
            path: 'secure',
            name: 'secure',
            component: Secure,
            meta: {
              requiresAuth: true
            }
          },
          {
              path: 'chat',
              name: 'chat',
              component: Chat,
              meta: {
                requiresAuth: true
              }
            },
            {
                path: '',
                name: 'home',
                component: Home,
                meta: {
                  requiresAuth: true
                }
              },
    ]
  },

];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.state.login.logged) {
      next()
      return
    }else{
      store.dispatch('checklogged').then(()=>{
          if (store.state.login.logged) {
              next()
              return
          }else {
              next({
                  path:'/login',
                  query: { redirect: to.fullPath }
              })
          }

      })

    }

  } else {
    next()
  }
})
export default router;
